//
//  MyView.swift
//  DemoCustomViews
//
//  Created by Apple on 24/07/17.
//  Copyright © 2017 dynamicarrowbits. All rights reserved.
//

import Foundation
import UIKit

class MyView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var label: UILabel!
    
    @IBAction func buttonTap(_ sender: UIButton) {
        //label.text = "Hi"
        print("button Pressed")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("MyView", owner: self, options: nil)
        self.addSubview(view)
        view.frame = self.bounds
    }
}
